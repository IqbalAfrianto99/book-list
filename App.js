/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import {Provider} from 'react-redux';

import Navigator from './src/shared/navigation';
import store from './src/shared/store/store';
import {RemotePushController} from './src/features';
// if (__DEV__) {
//   import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
// }

const App: () => React$Node = () => {
  return (
    <Provider store={store}>
      <Navigator />
      <RemotePushController />
    </Provider>
  );
};

export default App;
