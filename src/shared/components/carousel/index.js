import React from 'react';
import {View, FlatList} from 'react-native';

import SplitTitle from '../splitTitle';
import {styles} from './style';

const Carousel = ({
  title,
  button,
  onPressExpand,
  data,
  renderItem,
  keyExtractor,
}) => (
  <View style={styles.wrapper}>
    <SplitTitle title={title} button={button} onPress={onPressExpand} />
    <FlatList
      data={data}
      horizontal={true}
      renderItem={renderItem}
      keyExtractor={keyExtractor}
      showsHorizontalScrollIndicator={false}
    />
  </View>
);

export default Carousel;
