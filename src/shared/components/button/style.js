import {StyleSheet} from 'react-native';

import {hp} from '../../styles/styles';
import {Colors, Fonts} from '../../styles/index'

export const styles = StyleSheet.create({
  btnPrimary: {
    backgroundColor: Colors.primary,
    width: '100%',
    height: hp(7),
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: hp(4),
  },
  txtPrimary: {
    letterSpacing: 2,
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.medium,
    color: Colors.light,
  },
});
