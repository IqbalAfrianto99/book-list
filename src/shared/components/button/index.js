import React from 'react';
import {Text, TouchableOpacity, ActivityIndicator} from 'react-native';

import {styles} from './style';

class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <TouchableOpacity onPress={this.props.onPress} style={styles.btnPrimary}>
        {this.props.loading ? (
          <ActivityIndicator size="large" color="#fff" />
        ) : (
          <Text style={styles.txtPrimary}>{this.props.title}</Text>
        )}
      </TouchableOpacity>
    );
  }
}

export default Button;
