import {StyleSheet} from 'react-native';
import {AppStyle, wp, hp} from '../../styles/styles';

const {fontSize, fontSegoe, color} = AppStyle;

export const styles = StyleSheet.create({
  containerActive: {
    width: wp(80),
    borderColor: color.light,
    shadowColor: color.dark,
    shadowOffset: {
      width: 0,
      height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 5,
    elevation: 5,
    paddingHorizontal: wp(5),
    paddingVertical: hp(2),
    marginVertical: hp(1),
    borderRadius: 10,
  },
  containerInactive: {
    width: wp(80),
    paddingHorizontal: wp(5),
    paddingVertical: hp(2),
    marginVertical: hp(1),
  },
  marginText: {
    marginHorizontal: 10,
  },
  flexComponent: {
    flexDirection: 'row',
  },
  title: {
    marginHorizontal: wp(5),
    fontSize: fontSize.small,
    fontFamily: fontSegoe.regular,
    marginVertical: 0,
    padding: 0,
  },
  textInput: {
    marginHorizontal: wp(5),
    fontSize: fontSize.small,
    fontFamily: fontSegoe.bold,
    marginVertical: 0,
    padding: 0,
    width: wp(100),
    color: color.primary,
  },
  expandTitle: {
    marginLeft: wp(9.5),
    fontSize: fontSize.small,
  },
  icon: {marginTop: hp(0.2)},
});
