import React from 'react';
import {
  View,
  Text,
  TextInput,
  LayoutAnimation,
  Platform,
  TouchableOpacity,
  UIManager,
} from 'react-native';
import {styles} from './style';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import Colors from '../../styles/colors';
import Fonts from '../../styles/fonts';

class Input extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
    };
  }

  animationHandler = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({focus: !this.state.focus});
  };

  render() {
    if (
      Platform.OS === 'android' &&
      UIManager.setLayoutAnimationEnabledExperimental
    ) {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }

    return (
      <TouchableOpacity
        onPress={() => this.animationHandler()}
        style={
          this.state.focus ? styles.containerActive : styles.containerInactive
        }>
        {this.state.focus ? (
          <View>
            <Text style={styles.expandTitle}>{this.props.type}</Text>
            <View style={styles.flexComponent}>
              <Icon
                name={
                  this.props.type === 'EMAIL'
                    ? 'email'
                    : this.props.type === 'PASSWORD'
                    ? 'lock'
                    : this.props.type === 'NAME'
                    ? 'account-circle'
                    : this.props.type === 'PHONE'
                    ? 'phone'
                    : 'lock'
                }
                size={Fonts.size.medium}
                color={Colors.primary}
                style={styles.icon}
              />
              <TextInput
                placeholder={
                  this.props.value != null
                    ? this.props.value
                    : this.props.type === 'PASSWORD'
                    ? this.props.type
                    : this.props.type
                }
                value={this.props.value}
                onChangeText={(text) => this.props.onChangeText(text)}
                onSubmitEditing={() => this.setState({ focus: false })}
                blurOnSubmit={false}
                returnKeyType="next"
                placeholderTextColor={Colors.primary}
                style={styles.textInput}
                secureTextEntry={this.props.secureTextEntry}
                autoFocus={true}
              />
            </View>
          </View>
        ) : (
          <View style={styles.flexComponent}>
            <Icon
              name={
                this.props.type === 'EMAIL'
                  ? 'email'
                  : this.props.type === 'PASSWORD'
                  ? 'lock'
                  : this.props.type === 'NAME'
                  ? 'account-circle'
                  : this.props.type === 'PHONE'
                  ? 'phone'
                  : 'lock'
              }
              size={Fonts.size.medium}
              color={Colors.grey}
            />
            <Text style={styles.title}>
              {this.props.secureTextEntry && this.props.value !== null
                ? '*******'
                : !this.props.secureTextEntry && this.props.value !== null
                ? this.props.value
                : this.props.type}
            </Text>
          </View>
        )}
      </TouchableOpacity>
    );
  }
}

export default Input;
