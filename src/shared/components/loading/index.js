import React from 'react';
import {ActivityIndicator, Text, View} from 'react-native';

import styles from './style';
import Colors from '../../styles/colors';

const Loading = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator size="large" color={Colors.primary} />
      <Text>Loadin, please wait</Text>
    </View>
  );
};

export default Loading;
