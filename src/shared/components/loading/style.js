import {StyleSheet} from 'react-native';

import Fonts from '../../styles/fonts';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    marginTop: 12,
    fontSize: Fonts.size.medium,
    fontWeight: '700',
  },
});

export default styles;
