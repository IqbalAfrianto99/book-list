import React from 'react';
import {Text, View, TouchableOpacity} from 'react-native';

import {styles} from './style';

const splitTitle = ({title, button, onPress}) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>{title}</Text>
      <TouchableOpacity onPress={onPress}>
        <Text style={styles.btn}>{button}</Text>
      </TouchableOpacity>
    </View>
  );
};

export default splitTitle;
