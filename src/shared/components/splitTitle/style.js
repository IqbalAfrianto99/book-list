import {StyleSheet} from 'react-native';
import {hp} from '../../../shared/styles/styles';
import Colors from '../../../shared/styles/colors';
import Fonts from '../../../shared/styles/fonts';

export const styles = StyleSheet.create({
  btn: {
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.small,
    color: Colors.blue,
  },
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: hp(1.5),
  },
  title: {
    fontFamily: Fonts.type.medium,
    fontSize: Fonts.size.medium,
  },
});
