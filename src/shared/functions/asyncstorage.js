import AsyncStorage from '@react-native-async-storage/async-storage';

/**
 * Get authorization headers
 * @returns {Object} Authorization object
 */
export async function getHeaders() {
  const token = await AsyncStorage.getItem('APP_AUTH_TOKEN');

  return {
    'Content-Type': 'application/json',
    Authorization: 'Bearer ' + token,
  };
}

/**
 * Save token to AsyncStorage
 * @param {String} token Token string
 * @returns {Void}
 */
export async function saveToken(token) {
  AsyncStorage.setItem('APP_AUTH_TOKEN', token);
}

/**
 * Get auth token
 * @returns {String} Token
 */
export async function getAuthToken() {
  return await AsyncStorage.getItem('APP_AUTH_TOKEN');
}
