const config = {
  screens: {
    // Dashboard: {
    //   path: 'home/:id',
    //   parse: {
    //     id: (id) => `${id}`,
    //   },
    // },
    // Profile: {
    //   path: 'profile/:id',
    //   parse: {
    //     id: (id) => `${id}`,
    //   },
    // },
    // Notifications: 'notifications',
    // Settings: 'settings',
    dashboard: 'dashboard',
    bookDetail: 'book/:id',
  },
};

const linking = {
  prefixes: ['demo://booklist', 'https://booklist.com'],
  config,
};

export default linking;
