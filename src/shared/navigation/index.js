import React, {useState, useEffect, useRef} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {useSelector} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';
import analytics from '@react-native-firebase/analytics';

import {Login, Register, Dashboard, DetailBook} from '../../features';
import {navigationRef} from '../../features/notification/RootNavigation';
import {styles} from './style';
import linking from './linking';
import {getValue} from '../helpers/remoteconfig';

const Stack = createStackNavigator();

const configBookDetail = {
  title: 'Book Detail',
  headerTitleStyle: {textAlign: 'center'},
  style: {shadowColor: 'transparent'},
  headerStyle: styles.header,
};

let configDashboard = {
  headerTitleStyle: styles.headerTitle,
  style: {shadowColor: 'transparent'},
  headerTintColor: '#222',
  headerRight: () => (
    <Icon
      name="filter-alt"
      size={30}
      color="#9ea7aa"
      style={styles.filterIcon}
    />
  ),
  headerStyle: {
    backgroundColor: '#f2f2f2',
    elevation: 0,
  },
};

const configRegister = {
  title: false,
  style: {shadowColor: 'transparent'},
  headerTintColor: '#05ba74',
  headerStyle: styles.header,
};

/**
 * Main app navigation
 */
const Navigator = () => {
  const routeNameRef = useRef();
  const isLoggedIn = useSelector((state) => state.auth.isLoggedIn);
  const [title, setTitle] = useState('Venus Bookstore');

  const fetchTitle = async () => {
    const config = await getValue('app_title');
    setTitle(config._value);
  };

  useEffect(() => {
    configDashboard.title = title;
  }, [title]);

  useEffect(() => {
    fetchTitle();
  }, []);
  const handleNavigationChange = async () => {
    const previousRouteName = routeNameRef.current;
    const currentRouteName = navigationRef.current.getCurrentRoute().name;

    if (previousRouteName !== currentRouteName) {
      // The line below uses the expo-firebase-analytics tracker
      // https://docs.expo.io/versions/latest/sdk/firebase-analytics/
      // Change this line to use another Mobile analytics SDK
      await analytics().logScreenView({
        screen_name: currentRouteName,
        screen_class: currentRouteName,
      });
    }

    // Save the current route name for later comparison
    routeNameRef.current = currentRouteName;
  };

  return (
    <NavigationContainer
      ref={navigationRef}
      linking={linking}
      onStateChange={handleNavigationChange}>
      <Stack.Navigator>
        {isLoggedIn ? (
          <>
            <Stack.Screen
              name="dashboard"
              component={Dashboard}
              options={configDashboard}
              path="dashboard"
            />
            <Stack.Screen
              name="bookDetail"
              component={DetailBook}
              options={configBookDetail}
              path="bookDetail"
            />
          </>
        ) : (
          <>
            <Stack.Screen
              name="auth"
              component={Auth}
              options={{headerShown: false}}
            />
          </>
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

/**
 * Authentication stack
 * @param {any} props
 */
const Auth = (props) => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="login"
        component={Login}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="register"
        component={Register}
        options={configRegister}
      />
    </Stack.Navigator>
  );
};

export default Navigator;
