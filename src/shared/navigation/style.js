import {StyleSheet} from 'react-native';
import Fonts from './../styles/fonts';

export const styles = StyleSheet.create({
  filterIcon: {
    marginHorizontal: 10,
  },
  header: {
    backgroundColor: '#f2f2f2',
    elevation: 0,
  },
  headerTitle: {
    fontFamily: Fonts.type.medium,
    textAlign: 'center',
    marginLeft: 56,
  },
});
