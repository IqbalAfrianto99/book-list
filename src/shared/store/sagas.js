import {all} from 'redux-saga/effects';

import saga_login from '../../features/login/saga_login';
import saga_dashboard from '../../features/dashboard/saga_dashboard';

export default function* rootSaga() {
  yield all([saga_login(), saga_dashboard()]);
}
