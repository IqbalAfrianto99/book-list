import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';
import Reactotron from 'reactotron-react-native';

import rootReducer from './reducers';
import rootSaga from './sagas';

const sagaMonitor = Reactotron.createSagaMonitor;

const sagaMiddleware = createSagaMiddleware({sagaMonitor});
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware));

sagaMiddleware.run(rootSaga);

export default store;
