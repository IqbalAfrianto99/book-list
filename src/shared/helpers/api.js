import axios from 'axios';

/**
 * API Instance
 * BASE_URL = https://us-central1-principal-my.cloudfunctions.net/books/
 */
const APIRequestConstructor = () => {
  const BASE_URL = 'https://us-central1-principal-my.cloudfunctions.net/books/';

  const config = (token, contentType) => {
    const config = {
      // method,
      headers: {
        'Content-Type': contentType || 'application/json',
        Authorization: `Bearer ${token}` || '',
        'Accept-Language': 'en',
      },
    };

    return config;
  };

  const errorResponseDefault = {
    status: 500,
    message: 'Terjadi kesalahan',
  };
  /**
   * Perform GET request
   * @param {string} url Endpoint
   * @param {string} token Authentication
   */
  const get = async (url, token) => {
    let result;
    const configRequest = config(token);
    try {
      const res = await axios.get(`${BASE_URL}${url}`, configRequest);
      result = res.data;
    } catch (e) {
      result = e.response.data ?? errorResponseDefault;
    }

    return result;
  };

  /**
   * Perform POST request
   * @param {string} url Endpoint
   * @param {Object} data Payload
   * @param {string} token Authentication
   * @param {string} contentType Content type, default json
   */
  const post = async (url, data, token, contentType) => {
    let result;
    const configRequest = config(token, contentType);

    try {
      const res = await axios.post(`${BASE_URL}${url}`, data, configRequest);
      result = res.data;
    } catch (e) {
      result = e.response.data ?? errorResponseDefault;
    }

    return result;
  };

  /**
   * Perform a PUT Request
   * @param {string} url Endpoint
   * @param {Object} data Payload
   * @param {string} token Authentication
   * @param {string} contentType Content type, default json
   */
  const put = async (url, data, token, contentType) => {
    let result;
    const configRequest = config(token, contentType);

    try {
      const res = await axios.put(`${BASE_URL}${url}`, data, configRequest);
      result = res.data;
    } catch (e) {
      result = e.response.data ?? errorResponseDefault;
    }

    return result;
  };

  /**
   * Perform a DELETE Request
   * @param {string} url Endpoint
   * @param {string} token Authentication
   * @param {string} contentType Content type
   */
  const remove = async (url, token, contentType) => {
    let result, res;
    const configRequest = config(token, contentType);

    try {
      res = await axios.delete(`${BASE_URL}${url}`, configRequest);
      result = res.data;
    } catch (e) {
      result = e.response.data ?? errorResponseDefault;
    }

    return result;
  };

  return {get, post, put, remove};
};

export default APIRequestConstructor;
