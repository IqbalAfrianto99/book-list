import remoteConfig from '@react-native-firebase/remote-config';

const getValue = async (key) => {
  await remoteConfig().fetch(0);
  await remoteConfig().activate();
  return await remoteConfig().getValue(key);
};

const getAll = async () => {
  await remoteConfig().fetch(0);
  await remoteConfig().activate();
  return await remoteConfig().getAll();
}

export {getValue, getAll};
