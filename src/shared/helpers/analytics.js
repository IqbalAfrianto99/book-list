import analytics from '@react-native-firebase/analytics';

/**
 * Send custom event to firebase analytics
 * @param {String} key event key/name
 * @param {Object} data Object
 * @returns Void
 */
const logEvent = async (key, data) => {
  return await analytics().logEvent(key, data);
};

export {logEvent};
