const type = {
  light: 'Poppins-light',
  regular: 'Poppins-Regular',
  medium: 'Poppins-SemiBold',
  bold: 'Poppins-Bold',
  extraBold: 'Poppins-ExtraBold',
};

const size = {
  h1: 38,
  h2: 34,
  h3: 30,
  h4: 26,
  h5: 20,
  h6: 19,
  tiny: 8,
  small: 12,
  regular: 14,
  medium: 16,
  large: 18,
  input: 14,
  button: 14,
};

const style = {
  h1: {
    fontFamily: type.base,
    fontSize: size.h1,
  },
  h2: {
    fontWeight: 'bold',
    fontSize: size.h2,
  },
  h3: {
    fontFamily: type.emphasis,
    fontSize: size.h3,
  },
  h4: {
    fontWeight: 'bold',
    fontSize: size.h4,
  },
  h5: {
    fontFamily: type.base,
    fontSize: size.h5,
  },
  h6: {
    fontFamily: type.emphasis,
    fontSize: size.h6,
  },
  lightSmall: {
    fontFamily: type.light,
    fontSize: size.small,
  },
  lightRegular: {
    fontFamily: type.light,
    fontSize: size.regular,
  },
  lightMedium: {
    fontFamily: type.light,
    fontSize: size.medium,
  },
  regular: {
    fontFamily: type.regular,
    fontSize: size.regular,
  },
  regularSmall: {
    fontFamily: type.regular,
    fontSize: size.small,
  },
  regularMedium: {
    fontFamily: type.regular,
    fontSize: size.medium,
  },
  regularLarge: {
    fontFamily: type.regular,
    fontSize: size.large,
  },
  medium: {
    fontFamily: type.medium,
    fontSize: size.medium,
  },
  mediumRegular: {
    fontFamily: type.medium,
    fontSize: size.regular,
  },
  mediumLarge: {
    fontFamily: type.medium,
    fontSize: size.large,
  },
  boldRegular: {
    fontWeight: 'bold',
    fontSize: size.regular,
  },
  boldMedium: {
    fontWeight: 'bold',
    fontSize: size.medium,
  },
  boldLarge: {
    fontWeight: 'bold',
    fontSize: size.large,
  },
  description: {
    fontFamily: type.base,
    fontSize: size.medium,
  },
  input: {
    fontFamily: type.regular,
    fontSize: size.input,
  },
  button: {
    fontFamily: type.medium,
    fontSize: size.button,
  },
  tab: {
    fontFamily: type.regular,
    fontSize: size.small,
  },
  headerTitle: {
    fontFamily: type.medium,
    fontSize: size.large,
  },
};

export default {
  type,
  size,
  style,
};
