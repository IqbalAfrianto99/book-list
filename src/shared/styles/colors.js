const colors = {
  primary: '#05ba74',
  purple: '#bd72de',
  blue: '#44b0f9',
  red: '#f89f85',
  dark: '#000',
  light: '#fff',
  grey: '#9ea7aa',
};

export default colors;
