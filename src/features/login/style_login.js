import {StyleSheet} from 'react-native';

import {wp, fp} from '../../shared/styles/styles';
import {Fonts, Colors} from '../../shared/styles';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  container: {
    padding: wp(10),
    height: fp(100),
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgot: {
    color: Colors.primary,
    fontFamily: Fonts.type.medium,
  },
  forgotContainer: {
    marginLeft: 'auto',
  },
  formContainer: {
    marginVertical: 24,
    marginHorizontal: 12,
  },
  icon: {
    textAlign: 'center',
  },
  title: {
    fontFamily: Fonts.type.medium,
    fontSize: Fonts.size.h4,
    textAlign: 'center',
  },
  subtitle: {
    fontFamily: 'Poppins',
    color: Colors.grey,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
  },
  register: {
    fontFamily: Fonts.type.regular,
    color: Colors.primary,
    fontSize: Fonts.size.regular,
  },
  registerContainer: {
    fontFamily: Fonts.type.regular,
    marginTop: 12,
  },
});
