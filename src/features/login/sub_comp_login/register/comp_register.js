import React from 'react';
import {
  SafeAreaView,
  ScrollView,
  Text,
  KeyboardAvoidingView,
  Platform,
  View,
} from 'react-native';

import Button from '../../../../shared/components/button';
import Input from '../../../../shared/components/input';
import {styles} from './style_register';

export default class Register extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: null,
      name: null,
      phone: null,
      password: null,
      confirmPassword: null,
    };
  }

  componentDidUpdate(prevProps) {
    if (this.props.registerSuccess !== prevProps.registerSuccess) {
      this.props.navigation.pop();
    }
  }

  handleClickLogin = () => this.props.navigation.pop();
  handleRegister = () => {
    const data = {
      name: this.state.name,
      email: this.state.email,
      nip: this.state.phone,
      confirmPassword: this.state.confirmPassword,
      password: this.state.password,
    };
    this.props.registerAction(data);
  };
  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={styles.root}>
        <SafeAreaView style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.title}>Create Account</Text>
            <Text style={styles.subtitle}>Create a new account</Text>
            {/* Form */}

            <View style={styles.formContainer}>
              <Input
                type="NAME"
                value={this.state.name}
                onChangeText={(text) => this.setState({name: text})}
              />
              <Input
                type="EMAIL"
                value={this.state.email}
                onChangeText={(text) => this.setState({email: text})}
              />
              <Input
                type="PHONE"
                value={this.state.phone}
                onChangeText={(text) => this.setState({phone: text})}
              />
              <Input
                type="PASSWORD"
                value={this.state.password}
                onChangeText={(text) => this.setState({password: text})}
                secureTextEntry={true}
              />
              <Input
                type="CONFIRM PASSWORD"
                value={this.state.confirmPassword}
                onChangeText={(text) => this.setState({confirmPassword: text})}
                secureTextEntry={true}
              />
            </View>
            <Button
              loading={this.props.isLoading}
              title="REGISTER"
              onPress={this.handleRegister}
            />
            <Text style={styles.loginContainer}>
              Already have an account ?{' '}
              <Text onPress={this.handleClickLogin} style={styles.login}>
                Login
              </Text>
            </Text>
          </ScrollView>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}
