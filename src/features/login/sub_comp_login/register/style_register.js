import {StyleSheet} from 'react-native';

import {wp, fp} from '../../../../shared/styles/styles';
import {Fonts, Colors} from '../../../../shared/styles';

export const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
  container: {
    padding: wp(10),
    height: fp(100),
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  formContainer: {
    marginVertical: 24,
  },
  icon: {
    textAlign: 'center',
  },
  title: {
    fontWeight: 'bold',
    fontSize: Fonts.size.h4,
    textAlign: 'center',
  },
  subtitle: {
    color: Colors.grey,
    fontSize: Fonts.size.medium,
    textAlign: 'center',
  },
  login: {
    color: Colors.primary,
    fontSize: Fonts.size.regular,
  },
  loginContainer: {
    marginTop: 12,
  },
});
