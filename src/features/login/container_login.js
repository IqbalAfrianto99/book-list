import {connect} from 'react-redux';

import Login from './comp_login';
import {loginAction} from './action_login';

const mapStateToProps = (state) => {
  return {
    isLoading: state.auth.isLoading,
  };
};

const mapDispatchToProps = {
  loginAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
