import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  KeyboardAvoidingView,
  Platform,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/MaterialIcons';

import {styles} from './style_login';
import Button from '../../shared/components/button';
import Input from '../../shared/components/input';

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: 'admin@admin.com',
      password: 'admin2020',
    };
  }

  handleClickRegister = () => this.props.navigation.navigate('register');

  handleLogin = () => {
    const data = {
      email: this.state.email,
      password: this.state.password,
    };

    this.props.loginAction(data);
  };

  render() {
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        style={styles.root}>
        <SafeAreaView style={styles.container}>
          <Icon
            style={styles.icon}
            name="account-circle"
            size={100}
            color="rgba(34,34,34,0.4)" />
          <Text style={styles.title}>Welcome Back</Text>
          <Text style={styles.subtitle}>Sign in to continue</Text>
          {/* Form */}
          <View style={styles.formContainer}>
            <Input
              type="EMAIL"
              value={this.state.email}
              onChangeText={(text) => this.setState({email: text})}
            />
            <Input
              type="PASSWORD"
              value={this.state.password}
              onChangeText={(text) => this.setState({password: text})}
              secureTextEntry={true}
            />
            <View style={styles.forgotContainer}>
              <Text style={styles.forgot}>Forgot password?</Text>
            </View>
          </View>
          <Button
            loading={this.props.isLoading}
            title="LOGIN"
            onPress={this.handleLogin}
          />
          <Text style={styles.registerContainer}>
            Don't have an account ?{' '}
            <Text onPress={this.handleClickRegister} style={styles.register}>create a new account</Text>
          </Text>
        </SafeAreaView>
      </KeyboardAvoidingView>
    );
  }
}
