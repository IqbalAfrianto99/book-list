import {ToastAndroid} from 'react-native';
import {takeLatest, put} from 'redux-saga/effects';
import analytics from '@react-native-firebase/analytics';

// eslint-disable-next-line prettier/prettier
import {
  LOGIN_FAILED,
  REGISTER_FAILED,
  LOGIN,
  REGISTER,
  LOGIN_SUCCESS,
  REGISTER_SUCCESS,
} from './action_login';
import {BOOKS} from '../dashboard/action_dashboard';
import {loginApi, registerApi} from './url_login';
import {saveToken} from '../../shared/functions/asyncstorage';
import {logEvent} from '../../shared/helpers/analytics';

function* login(action) {
  try {
    const resLogin = yield loginApi(action.payload);
    if (resLogin && resLogin.data) {
      ToastAndroid.showWithGravity(
        'Login Berhasil',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield saveToken(resLogin.data.token);
      yield put({type: LOGIN_SUCCESS});
      yield put({type: BOOKS});
      yield analytics().logLogin({method: 'email'});
    } else {
      ToastAndroid.showWithGravity(
        resLogin.errors,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield put({type: LOGIN_FAILED});
      yield logEvent('login_failed', {err: resLogin.errors});
    }
  } catch (err) {
    console.log('err', err);
    yield put({type: LOGIN_FAILED});
    yield logEvent('login_failed', err);
  }
}

function* register(action) {
  try {
    const resRegis = yield registerApi(action.payload);

    yield analytics().logSignUp({method: 'email'});

    if (resRegis && resRegis.data) {
      ToastAndroid.showWithGravity(
        'Register Berhasil',
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield put({type: REGISTER_SUCCESS});
    } else {
      yield logEvent('signup_failed', {err: resRegis.errors});

      ToastAndroid.showWithGravity(
        resRegis.errors,
        ToastAndroid.SHORT,
        ToastAndroid.BOTTOM,
      );
      yield put({type: REGISTER_SUCCESS});
    }
  } catch (err) {
    console.log('err', err);
    yield logEvent('signup_failed', err);
    yield put({type: REGISTER_FAILED});
  }
}

export default function* saga_login() {
  yield takeLatest(LOGIN, login);
  yield takeLatest(REGISTER, register);
}
