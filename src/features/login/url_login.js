import api from '../../shared/helpers/api';

export const loginApi = (payload) => {
  return api().post('login', payload);
};

export const registerApi = (payload) => {
  return api().post('register', payload);
};
