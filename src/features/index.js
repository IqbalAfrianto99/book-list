import Dashboard from './dashboard/container_dashboard';
import Login from './login/container_login';
import Register from './login/sub_comp_login/register/container_register';
import DetailBook from './dashboard/sub_comp_dashboard/detail/container_detail';
import RemotePushController from './notification/services_notification';

export {Login, Register, Dashboard, DetailBook, RemotePushController};
