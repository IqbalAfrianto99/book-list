import {StyleSheet} from 'react-native';
import {wp, hp} from '../../shared/styles/styles';
import Colors from '../../shared/styles/colors';
import Fonts from '../../shared/styles/fonts';

export const styles = StyleSheet.create({
  author: {
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.small,
    marginVertical: hp(1),
  },
  bookCover: {
    width: wp(29),
    height: hp(25),
    borderRadius: 10,
    marginVertical: hp(1),
  },
  bookCoverInFocus: {
    width: wp(32),
    height: hp(25),
    borderRadius: 10,
    marginVertical: hp(1),
  },
  categories: {
    color: Colors.light,
    fontSize: Fonts.size.medium,
    fontFamily: Fonts.type.bold,
  },
  container: {
    flex: 1,
    marginHorizontal: 6,
  },
  containerBookListInFocus: {
    marginRight: wp(5),
    width: wp(30),
  },
  containerBookList: {
    marginHorizontal: wp(1),
    marginBottom: hp(2),
    width: wp(29),
  },
  containerCategories: {
    width: wp(40),
    height: hp(15),
    borderRadius: 10,
    marginHorizontal: wp(1),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: Fonts.type.medium,
    fontSize: Fonts.size.medium,
  },
  price: {
    color: Colors.primary,
    fontSize: Fonts.size.regular,
  },
});
