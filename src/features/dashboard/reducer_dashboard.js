import {
  BOOKS,
  BOOKS_SUCCESS,
  BOOKS_FAILED,
  BOOK_DETAIL,
  BOOK_DETAIL_SUCCESS,
  BOOK_DETAIL_FAILED,
} from './action_dashboard';

// InitialState
const initialState = {
  isLoading: false,
  books: null,
  topNewRelease: null,
  booksDetail: [],
};

const getBooks = (state = initialState, action) => {
  switch (action.type) {
    case BOOKS: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case BOOKS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        books: action.payload,
        topNewRelease: action.payload.slice(0, 5),
      };
    }
    case BOOKS_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    case BOOK_DETAIL: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case BOOK_DETAIL_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        booksDetail: action.payload,
      };
    }
    case BOOK_DETAIL_FAILED: {
      return {
        ...state,
        isLoading: false,
      };
    }
    default:
      return state;
  }
};

export default getBooks;
