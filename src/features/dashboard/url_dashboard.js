import API from '../../shared/helpers/api';

export const getBooksApi = (headers) => {
  return API().get('books', headers);
};

export const getBookDetailsApi = (id, headers) => {
  return API().get(`books/${id}`, headers);
};
