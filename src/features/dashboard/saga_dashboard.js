import {
  BOOKS,
  BOOKS_SUCCESS,
  BOOKS_FAILED,
  BOOK_DETAIL,
  BOOK_DETAIL_SUCCESS,
  BOOK_DETAIL_FAILED,
  BOOK_DETAIL_CLEAR,
} from './action_dashboard';
import {getBooksApi, getBookDetailsApi} from './url_dashboard';
import {takeLatest, put} from 'redux-saga/effects';
import {getAuthToken} from '../../shared/functions/asyncstorage';
import {logEvent} from '../../shared/helpers/analytics';

function* getBooks(action) {
  try {
    const token = yield getAuthToken();

    const resBooks = yield getBooksApi(token);

    if (resBooks && resBooks.data) {
      yield put({type: BOOKS_SUCCESS, payload: resBooks.data});
    } else {
      yield put({type: BOOKS_FAILED});
    }
  } catch (err) {
    console.log(err);
    yield put({type: BOOKS_FAILED});
  }
}

function* bookDetails(action) {
  try {
    const token = yield getAuthToken();
    const resBookDetail = yield getBookDetailsApi(action.payload, token);

    yield logEvent('book_detail', {id: action.payload});

    if (resBookDetail && resBookDetail.data) {
      yield put({type: BOOK_DETAIL_SUCCESS, payload: resBookDetail.data});
    } else {
      yield put({type: BOOK_DETAIL_FAILED});
    }
  } catch (err) {
    yield put({type: BOOK_DETAIL_FAILED});
    yield logEvent('book_detail_failed', err);
  }
}

function* clearBookDetail() {
  yield put({type: BOOK_DETAIL_CLEAR});
}

function* saga_dashboard() {
  yield takeLatest(BOOKS, getBooks);
  yield takeLatest(BOOK_DETAIL, bookDetails);
  yield takeLatest(BOOK_DETAIL_CLEAR, clearBookDetail);
}

export default saga_dashboard;
