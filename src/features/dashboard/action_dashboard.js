export const BOOKS = 'BOOKS';
export const BOOKS_SUCCESS = 'BOOKS_SUCCESS';
export const BOOKS_FAILED = 'BOOKS_FAILED';

export const BOOK_DETAIL = 'BOOK_DETAIL';
export const BOOK_DETAIL_SUCCESS = 'BOOK_DETAIL_SUCCESS';
export const BOOK_DETAIL_FAILED = 'BOOK_DETAIL_FAILED';
export const BOOK_DETAIL_CLEAR = 'BOOK_DETAIL_CLEAR';

export const getBooksAction = (payload) => {
  return {
    type: BOOKS,
    payload,
  };
};

export const bookDetailAction = (payload) => {
  return {
    type: BOOK_DETAIL,
    payload,
  };
};
