import React from 'react';
import {
  FlatList,
  Image,
  LayoutAnimation,
  SafeAreaView,
  Text,
  TouchableOpacity,
  UIManager,
  View,
} from 'react-native';

import Carousel from '../../shared/components/carousel';
import Colors from '../../shared/styles/colors';
import Loading from '../../shared/components/loading';
import SplitTitle from '../../shared/components/splitTitle';
import {styles} from './style_dashboard';

export default class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      categories: [
        'Drama',
        'Photography',
        'History',
        'Fantasy',
        'Science Fiction',
        'Biography',
      ],
      colors: [
        Colors.purple,
        Colors.blue,
        Colors.red,
        Colors.purple,
        Colors.blue,
        Colors.red,
      ],
      focus: false,
    };
  }

  bookDetailHandler = (item) => {
    this.props.bookDetailAction(item);
    this.props.navigation.navigate('bookDetail');
  };

  keyExtractorBooks = (item) => item.id;
  renderBooks = ({item}) => (
    <TouchableOpacity
      key={item.id}
      onPress={() => this.bookDetailHandler(item.id)}
      style={
        this.state.focus
          ? styles.containerBookList
          : styles.containerBookListInFocus
      }>
      <Image
        source={{uri: item.coverImageUrl}}
        style={this.state.focus ? styles.bookCover : styles.bookCoverInFocus}
      />
      <Text numberOfLines={1} style={styles.title}>
        {item.title}
      </Text>
      <Text numberOfLines={1} style={styles.author}>
        {item.author}
      </Text>
      <Text style={styles.price}>$ {item.price}</Text>
    </TouchableOpacity>
  );

  renderBooksList = (item) => (
    <React.Fragment key={item}>
      <SplitTitle
        title={this.state.focus ? 'All Books' : 'Top New Release'}
        button={this.state.focus ? 'View Less' : 'View All'}
        onPress={this.animationHandler}
      />
      <FlatList
        key={this.state.focus ? 1 : 0}
        data={this.state.focus ? this.props.data : this.props.topNewRelease}
        numColumns={this.state.focus ? 3 : false}
        horizontal={this.state.focus ? false : true}
        renderItem={this.renderBooks}
        keyExtractor={this.keyExtractorBooks}
        showsHorizontalScrollIndicator={false}
        showsVerticalScrollIndicator={false}
      />
    </React.Fragment>
  );

  keyExtractorCategories = (item) => item;
  renderCategories = ({item, index}) => (
    <View
      key={item}
      style={{
        ...styles.containerCategories,
        backgroundColor: this.state.colors[index],
      }}>
      <Text style={styles.categories}>{item}</Text>
    </View>
  );

  renderCategoryList = (item) => (
    <React.Fragment key={item}>
      <Carousel
        data={this.state.categories}
        renderItem={this.renderCategories}
        keyExtractor={this.keyExtractorCategories}
        title="Top Categories"
        button="VIEW ALL"
      />
    </React.Fragment>
  );

  renderNewReleaseList = (item) => (
    <React.Fragment key={item}>
      <Carousel
        data={this.props.topNewRelease}
        renderItem={this.renderBooks}
        keyExtractor={this.keyExtractorBooks}
        title="Top Categories"
        button="VIEW ALL"
      />
    </React.Fragment>
  );

  // Prevent using flatlist inside scrollview
  renderContents = ({item, index}) => {
    switch (item) {
      case 'books':
        return this.renderBooksList(item);
      case 'categories':
        return this.renderCategoryList(item, index);
      case 'new_release':
        return this.renderNewReleaseList(item);
      default:
        break;
    }
  };

  animationHandler = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    this.setState({focus: !this.state.focus});
  };
  render() {
    const {isLoading} = this.props;

    return (
      <SafeAreaView style={styles.container}>
        {isLoading ? (
          <Loading />
        ) : (
          <FlatList
            key={543234}
            keyExtractor={(item) => item.index}
            data={['books', 'categories', 'new_release']}
            renderItem={this.renderContents}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
          />
        )}
      </SafeAreaView>
    );
  }
}
