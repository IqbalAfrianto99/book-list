import React from 'react';
import {Rating} from 'react-native-ratings';
import {Image, SafeAreaView, ScrollView, Text, View} from 'react-native';

import styles from './style_detail';
import Loading from '../../../../shared/components/loading';

export default class Detail extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {params} = this.props.route;

    if (params?.id) {
      this.props.bookDetailAction(params.id);
    }
  }

  render() {
    const {coverImageUrl, title, author, synopsis, rating} = this.props.data;
    return (
      <SafeAreaView style={styles.container}>
        {this.props.loading ? (
          <Loading />
        ) : (
          <ScrollView showsVerticalScrollIndicator={false}>
            <Image source={{uri: coverImageUrl}} style={styles.cover} />
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.author}>{author}</Text>
            <View style={styles.ratingContainer}>
              <Rating
                ratingCount={5}
                startingValue={rating?.toString()}
                imageSize={20}
                readonly
                tintColor={'#f2f2f2'}
              />
              <Text style={styles.review}>38 Reviews</Text>
            </View>
            <View style={styles.synopsisContainer}>
              <Text style={styles.synopsis}>{synopsis}</Text>
            </View>
          </ScrollView>
        )}
      </SafeAreaView>
    );
  }
}
