import {StyleSheet} from 'react-native';

import {wp, hp} from '../../../../shared/styles/styles';
import Fonts from '../../../../shared/styles/fonts';
import Colors from '../../../../shared/styles/colors';

const styles = StyleSheet.create({
  author: {
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.medium,
    color: Colors.grey,
    textAlign: 'center',
  },
  container: {
    flex: 1,
    marginHorizontal: 12,
    marginVertical: 10,
  },
  cover: {
    width: wp(50),
    height: hp(40),
    alignSelf: 'center',
    resizeMode: 'cover',
    borderRadius: 8,
  },
  title: {
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.large,
    textAlign: 'center',
    marginTop: 12,
    marginBottom: 6,
  },
  synopsis: {
    fontFamily: Fonts.type.regular,
    fontSize: Fonts.size.regular,
    padding: hp(2),
  },
  synopsisContainer: {
    marginTop: hp(4),
    borderTopColor: Colors.grey,
    borderTopWidth: 0.5,
  },
  ratingContainer: {
    marginTop: hp(2),
    flexDirection: 'row',
    justifyContent: 'center',
  },
  review: {
    fontFamily: Fonts.type.regular,
    fontWeight: 'bold',
    marginLeft: 6,
    color: Colors.blue,
  },
});

export default styles;
