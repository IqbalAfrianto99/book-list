import {connect} from 'react-redux';

import Detail from './comp_detail';
import {bookDetailAction, clearBookDetail} from '../../action_dashboard';

const mapStateToProps = (state) => {
  return {
    data: state.getBooks.booksDetail,
    loading: state.getBooks.isLoading,
  };
};

const mapDispatchToProps = {
  bookDetailAction,
  clearBookDetail,
};

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
