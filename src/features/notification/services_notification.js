import {useEffect} from 'react';
import PushNotification from 'react-native-push-notification';
import {useDispatch} from 'react-redux';
import * as RootNavigation from './RootNavigation';

const RemotePushController = () => {
  const dispatch = useDispatch();

  const bookDetailHandler = (data) => {
    dispatch({type: 'BOOK_DETAIL', payload: data});
    RootNavigation.navigate('bookDetail');
  };

  useEffect(() => {
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      // onRegister: function (token) {
      //   console.log('TOKEN:', token);
      // },
      // (required) Called when a remote or local notification is opened or received
      onNotification: function (notification) {
        const action = notification.userInteraction;
        if (action) {
          const id = notification.data.bookId;
          bookDetailHandler(id);
        }
      },
      // Android only: GCM or FCM Sender ID
      senderID: '331425879788', // Sender ID from FCM
      popInitialNotification: true,
      requestPermissions: true,
    });
  }, []);

  return null;
};
export default RemotePushController;
